# mediconseil

## Project setup
```
npm install
```

### Compiles and hot-reloads for development

#### Run docker
```
npm run docker
```

#### Run frontend
```
npm run front
```

#### Run backend
```
npm run back
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```