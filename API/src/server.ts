import fastify, { FastifyInstance } from "fastify";
import fastifyCookie from "@fastify/cookie";
import { mysqlClose, mysqlInit } from "./db";
import userPlugin from "./user";
import env from "@env";
import { getUser } from "./db/user";
import CORS from 'fastify-cors';


const buildServer = async (): Promise<FastifyInstance> => {
    await mysqlInit()
    const server = fastify({ logger: true})

    server.decorateRequest('USERID', null) //decorate requests with userid

    //Server-wide hook 
    server.addHook('preHandler',  async (req, res) => { //auth
        const signedCookieValue = req.cookies.USERID //check for auth cookie
        if (!signedCookieValue) return // Let request through if there's no cookie
        const userId = !!signedCookieValue ? req.unsignCookie(signedCookieValue) : {valid: false, renew: false, value: ''};
        if (!userId.valid || !userId.value) {
          res.status(400).send("Provided auth cookie is invalid.");
          return;
        }
        // Check database
        const usr = await getUser(userId.value);
        if (!usr) {
          return res.status(401).send("Unauthorized, user is unknown")
        } 
        req.userId = userId.value
      });



    // server.get('/ping', (_, res) => {
    //     return res.status(200).send("pong")
    // })

    await server.register(fastifyCookie, {
      secret: env.AUTH_COOKIE_SECRET,
      parseOptions: {}
    })
    await server.register(CORS, {origin: true, credentials: true});
    await server.register(userPlugin, {prefix: '/user'})

    server.addHook('onClose', async (instance, done) => { //cleanup
        await mysqlClose();
        done()
      });
    return server
}

export default buildServer;