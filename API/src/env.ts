//env module to handle defaults
export default {
    PORT: process.env.PORT ? parseInt(process.env.PORT) : 8080,

    MYSQL_HOST: process.env.MYSQL_HOST ?? "localhost",
    MYSQL_PORT: process.env.MYSQL_PORT ? parseInt(process.env.MYSQL_PORT) : 3306,
    MYSQL_DB: process.env.MYSQL_DB ?? "database",
    MYSQL_USER: process.env.MYSQL_USER ?? "user",
    MYSQL_PASS: process.env.MYSQL_PASS ?? "pass",
    API_URL:  process.env.API_URK ?? 'localhost',
    AUTH_COOKIE_SECRET: process.env.AUTH_COOKIE_SECRET ?? "supersecretkey",
}
  