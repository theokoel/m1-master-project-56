import { PassThrough } from 'stream';
import {connection} from '.'

export const getUser = async (username: string): Promise<DbUser|undefined> => {
    const [rows, _] = await connection.query('SELECT * FROM user WHERE username = ?', [username]);
    return (rows as (DbUser | undefined)[] ) [0]
}


export const deleteUser = async (id: number): Promise<void> => {
  await connection.query('DELETE FROM user WHERE id = ?', [id])
}

export const createUser = async (username: string, hash: string): Promise<void> => {
    console.log(username, hash)
    await connection.query(
    `INSERT INTO
        user (username, hash)
      VALUES
        (?, ?)`, [username, hash])
}