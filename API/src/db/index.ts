import env from '@env';
import mysql from 'mysql2/promise';

export let connection: mysql.Connection;

export const mysqlInit = async () => {
  connection = await mysql.createConnection({
    host: env.MYSQL_HOST,
    database: env.MYSQL_DB,
    user: env.MYSQL_USER,
    password: env.MYSQL_PASS,
    port: env.MYSQL_PORT
  });
  await connection.connect()
  console.log(" : [DB] Database connection initialized.")

}

export const mysqlClose = async () => {
  console.log(" : [DB] Database connection closed.")
  return connection.end();
}
