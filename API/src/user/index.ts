import type { FastifyPluginCallback, RequestGenericInterface } from "fastify";
import {hash, compare} from 'bcrypt'
import { createUser, getUser } from"../db/user"
import type { CookieSerializeOptions } from "@fastify/cookie";
import env from "@env";


const authCookieOptions : CookieSerializeOptions = {
    signed: true,
    domain: env.API_URL,
    path: '/',
    secure: true,
    sameSite: 'lax'
  }

const userPlugin: FastifyPluginCallback = async (app, _, done) => {

    await app.register(async function AuthRoutes(auth) {
        app.addHook<BaseUserRequest>('preHandler', async (req, res) => {
            if (!req.body || !req.body.username || !req.body.password) {
                return res.status(400).send('Missing credentials')
            }
        })

        auth.post<BaseUserRequest>('/register', async (req, res) => {
            try{
                if (!!await getUser(req.body.username)){
                    return res.status(400).send('User already exists')
                }
                hash(req.body.password, 10, async (err, hash) => {
                    if (err) throw err
                    await createUser(req.body.username, hash)
                })
                return res.status(200).send('ok')
            } catch(e) {
                return res.status(500).send(`Something went wrong creating user : ${e}`)
            }
        })
    
        auth.post<BaseUserRequest>('/login', async (req, res) => {
            try{
                const user = await getUser(req.body.username)
                if (!user) {
                    return res.status(400).send('User does not exist')
                }
                if ( !await compare(req.body.password, user.hash)) {
                    return res.status(401).send('Wrong credentials')
                }
                res.setCookie('USERID', user.id.toString(), authCookieOptions)
                return res.status(200).send('ok')
            } catch(e) {
                return res.status(500).send(`Something went wrong creating user : ${e}`)
            }
        })
    })

    app.get('/logout', (req, res) => {
        if (!req.userId){
          res.status(400).send('Cookie not included') //we need to get the auth cookie to be able to remove it
          return
        }
        res.clearCookie('USERID', {path: '/'});
        res.setCookie('USERID', 'id', {...authCookieOptions, expires: new Date(0)});
        res.status(200).send({message : 'ok'})
      });

    done();
}

export default userPlugin;