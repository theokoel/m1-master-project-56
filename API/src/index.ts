import env from '@env'
import buildServer from './server';

const start = async () => {
  const server = await buildServer();
  server.listen({port: env.PORT})
  console.log(`[FS] listening on port ${env.PORT}`)
}

start()