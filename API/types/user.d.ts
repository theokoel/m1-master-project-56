interface BaseUserRequest extends RequestGenericInterface {
    Body: {
        username: string,
        password: string
    }
}

interface DbUser {
    id: number,
    username: string,
    hash: string,
}
  
