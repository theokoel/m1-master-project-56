import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import QuestionView from '../views/QuestionView.vue'
import VerdictView from '../views/VerdictView.vue'
import Login from '../views/LoginView.vue'
import Register from '../views/RegisterView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/questions',
    name: 'QuestionView',
    component: QuestionView
  },
  {
    path: '/verdict',
    name: 'Verdict',
    component: VerdictView
  },
  {
    path: '/login',
    name: 'LoginView',
    component: Login
  },
  {
    path: '/register',
    name: 'RegisterView',
    component: Register
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
