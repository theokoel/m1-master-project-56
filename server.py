from unicodedata import name
from flask import Flask, render_template, request, jsonify
from flask_cors import CORS
import random
import numpy as np
import os
import mysql.connector
import random

# configuration
DEBUG = True

app = Flask(__name__)
app.config.from_object(__name__)
            # static_folder = "./dist/static",
            # template_folder = "./dist")

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

# To put in another file if got time ;)
MYSQL_HOST = os.environ.get("MYSQL_HOST") or "localhost"
MYSQL_PORT = os.environ.get("MYSQL_PORT") or 3306
MYSQL_DB = os.environ.get("MYSQL_DB") or "database"
MYSQL_USER = os.environ.get("MYSQL_USER") or "user"
MYSQL_PASS = os.environ.get("MYSQL_PASS") or "pass"

sql = mysql.connector.connect(
    user = MYSQL_USER,
    password = MYSQL_PASS,
    host = MYSQL_HOST,
    port = MYSQL_PORT,
    database = MYSQL_DB
)

cursor = sql.cursor()

#QUERIES FOR THE FORM AND THE DISEASE PREDICTOR

query = ("SELECT * FROM disease")
cursor.execute(query)

symptomsResult = cursor.fetchall()

# Take 10 random number between 0 and result length
randomNumber = []
randomSymptoms = []

for i in range(10):
    num = random.randint(0,len(symptomsResult)-1)
    while (num in randomNumber):
        num = random.randint(0,len(symptomsResult)-1)
    randomNumber.append(num)

questions = {}

for element in randomNumber:
    randomSymptoms.append(symptomsResult[element])
    questions[element] = "Do you have " + symptomsResult[element][0] + "?"

query = ("SELECT COLUMN_NAME, ORDINAL_POSITION FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'disease'")
cursor.execute(query)

diseasesResult = cursor.fetchall()

diseases = []

for value in range(len(diseasesResult)):
    
    if not diseasesResult[value][0] == "Symptoms":
        disease = {}
        disease['name'] = diseasesResult[value][0].lower()
        
        answer = {}
        for values2 in range(len(randomNumber)):
            answer[randomNumber[values2]] = randomSymptoms[values2][(diseasesResult[value][1])-1]

        disease['answers'] = answer
        diseases.append(disease)




# QUERIES FOR THE PRECAUTION TO TAKE
query=("SELECT * FROM traitement")
cursor.execute(query)
traitement=cursor.fetchall()
Precverdict={}
for i in range(41):
    precau=[]
    for j in range(len(traitement[0])-2):
        precau.append(traitement[i][j+2])
    Precverdict[traitement[i][0].lower()]=precau

# For debug
# print(questions)
# print(diseases)

# all close
cursor.close()
sql.close()

questions_so_far = []
answers_so_far = []


# @app.route('/traitement')
# def treat():
#     verdict = request.args.get('diseaseVerdict')
#     return None


@app.route('/test')

def index():
    global questions_so_far, answers_so_far

    question = request.args.get('question')
    answer = request.args.get('answer')
    if question and answer:
        questions_so_far.append(int(question))
        answers_so_far.append(float(answer))

    probabilities = calculate_probabilites(questions_so_far, answers_so_far)
    print("probabilities", probabilities)

    questions_left = list(set(questions.keys()) - set(questions_so_far))
    if len(questions_left) == 0:
        result = sorted(
            probabilities, key=lambda p: p['probability'], reverse=True)[0]
        precaution_1 = Precverdict[result['name']][0]
        precaution_2 = Precverdict[result['name']][1]
        precaution_3 = Precverdict[result['name']][2]
        precaution_4 = (Precverdict[result['name']][3] if len(Precverdict[result['name']])>3 else None)
        return jsonify(status="True", result={"result":result['name'],
            'precaution_1': precaution_1,
            'precaution_2': precaution_2,
            'precaution_3': precaution_3,
            'precaution_4': precaution_4
            })
        #return render_template('index.html', result=result['name'])
    else:
        next_question = random.choice(questions_left)
        return jsonify(status="True",result={"question":next_question, "question_text":questions[next_question]})
        #return render_template('index.html', **{"question":next_question}, **{"question_text":questions[next_question]})


def calculate_probabilites(questions_so_far, answers_so_far):
    probabilities = []
    for character in diseases:
        probabilities.append({
            'name': character['name'],
            'probability': calculate_character_probability(character, questions_so_far, answers_so_far)
        })

    return probabilities

def calculate_character_probability(character, questions_so_far, answers_so_far):
    # Prior
    P_character = 1 / len(diseases)

    # Likelihood
    P_answers_given_character = 1
    P_answers_given_not_character = 1
    for question, answer in zip(questions_so_far, answers_so_far):
        P_answers_given_character *= max(
            1 - abs(answer - character_answer(character, question)), 0.01)

        P_answer_not_character = np.mean([1 - abs(answer - character_answer(not_character, question))
                                          for not_character in diseases
                                          if not_character['name'] != character['name']])
        P_answers_given_not_character *= max(P_answer_not_character, 0.01)

    # Evidence
    P_answers = P_character * P_answers_given_character + \
        (1 - P_character) * P_answers_given_not_character

    # Bayes Theorem
    P_character_given_answers = (
        P_answers_given_character * P_character) / P_answers

    return P_character_given_answers


def character_answer(character, question):
    if question in character['answers']:
        return character['answers'][question]
    return 0.5

if __name__ == '__main__':
    app.run()
